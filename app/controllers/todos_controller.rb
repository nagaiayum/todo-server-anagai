class TodosController < ApplicationController
  before_action :set_todo, only: [:update, :destroy]

  REQUEST_SUCCEEDED_ERROR_CODE = 0
  INDEX_REQUEST_FAILED_ERROR_CODE = 3
  CREATE_REQUEST_FAILED_ERROR_CODE = 4
  UPDATE_REQUEST_FAILED_ERROR_CODE = 5
  DESTROY_REQUEST_FAILED_ERROR_CODE = 6

  # GET /todos
  def index
    todos = Todo.select(:id, :title, :detail, :date).order(:date)
    render json: base_request_body.merge({ todos: todos }), status: 200
  rescue ActiveRecord::ActiveRecordError
    render json: base_request_body(error_code: INDEX_REQUEST_FAILED_ERROR_CODE, error_message: '一覧の取得に失敗しました'), status: 500
  end

  # POST /todos
  def create
    Todo.create!(todo_params)
    render_succeeded_request
  rescue ActiveRecord::RecordInvalid
    render_bad_request
  rescue ActiveRecord::ActiveRecordError
    render json: base_request_body(error_code: CREATE_REQUEST_FAILED_ERROR_CODE, error_message: '登録に失敗しました'), status: 500
  end

  # PUT /todos/:id
  def update
    @todo.update!(todo_params)
    render_succeeded_request
  rescue ActiveRecord::RecordInvalid
    render_bad_request
  rescue ActiveRecord::ActiveRecordError
    render json: base_request_body(error_code: UPDATE_REQUEST_FAILED_ERROR_CODE, error_message: '更新に失敗しました'), status: 500
  end

  # DELETE /todos/:id
  def destroy
    @todo.destroy!
    render_succeeded_request
  rescue ActiveRecord::ActiveRecordError
    render json: base_request_body(error_code: DESTROY_REQUEST_FAILED_ERROR_CODE, error_message: '削除に失敗しました'), status: 500
  end

  private

  def todo_params
    params.permit(:title, :detail, :date)
  end

  def set_todo
    @todo = Todo.find(params[:id])
  end

  def render_succeeded_request
    render json: base_request_body ,status: 200
  end

  def base_request_body(error_code: REQUEST_SUCCEEDED_ERROR_CODE, error_message: '')
    { error_code: error_code, error_message: error_message }
  end
end