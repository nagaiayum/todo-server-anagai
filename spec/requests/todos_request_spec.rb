require 'rails_helper'

RSpec.describe "Todos", type: :request do
  let!(:todos) { create_list(:todo, 10) }
  let(:todo_id) { todos.first.id }

  describe 'GET /todos' do
    context 'When the list is successfully retrieved' do
      it 'if a success response, returned status 200 and error_code_0' do
        get '/todos'
        expect(response.status).to eq 200
        expect(json).to be_present
        expect(json['todos'].size).to eq todos.count
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'When it fails to get the list' do
      it 'return status 500 and error_code_3' do
        allow(Todo).to receive(:select).and_raise(ActiveRecord::ActiveRecordError)
        get '/todos'
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 3
        expect(json['error_message']).to eq '一覧の取得に失敗しました'
      end
    end

    context 'When an unknown error' do
      it 'return status 500 and error_code_1' do
        allow(Todo).to receive(:select).and_raise(Exception)
        get '/todos'
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end

  describe 'POST /todos' do
    let(:valid_attributes) { { title: 'test', detail: 'test', date: '2020-06-01' } }
    let(:invalid_attributes) { { title: '', detail: 'test', date: '2020-06-01' } }

    context 'When the request is normal' do
      it 'if a return the succesflly responce, return the status 200 and error_code_0' do
        post '/todos', params: valid_attributes
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'When no title is written' do
      it 'Todo update failed' do
        put "/todos/#{todo_id}", params: invalid_attributes
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'When the request success and title of 100 characters' do
      let(:params) { { title: 't' * 100 } }

      it 'if return the successfully responce return status 200 and error_code_0' do
        post '/todos', params: params
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'When the request success and detail of 1000 characters' do
      let(:params) { { title: 'test', detail: 't' * 1000 } }

      it 'if return the successfully responce return status 200 and error_code_0' do
        post '/todos', params: params
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'When a request is unusual' do
      it 'return the status 400 and error_code_2' do
        post '/todos', params: invalid_attributes
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'When a request is unusual and title of 101 or more characters' do
      let(:params){ { title: 't' * 101 } }

      it 'return the status 400 and error_code_2' do
        post '/todos', params: params
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'When a request is unusual and detail of 1001 or more characters' do
      let(:params){ { title: 'test', detail: 't' * 1001 } }

      it 'return the status 400 and error_code_2' do
        post '/todos', params: params
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'When registration fails' do
      it 'return status 500 and error_code_4' do
        allow(Todo).to receive(:create!).and_raise(ActiveRecord::ActiveRecordError)
        post '/todos', params: valid_attributes
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 4
        expect(json['error_message']).to eq '登録に失敗しました'
      end
    end

    context 'When an unknown error occurs' do
      it 'if a server error return the error_code_1' do
        allow(Todo).to receive(:create!).and_raise(Exception)
        post '/todos', params: valid_attributes
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end

  describe 'PUT /todos/:id' do
    let(:valid_attributes) { { title: 'ayumun' } }

    context 'When the request successfully' do
      it 'if successfully responce return status 200 and error_code_0' do
        put "/todos/#{todo_id}", params: valid_attributes
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'When the request success and title 100 characters' do
      let(:params) { { title: 't' * 100 } }

      it 'if successfully responce return status 200 and error_code_0' do
        put "/todos/#{todo_id}", params: params
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'When the request successfully and detail 1000 characters' do
      let(:params) { { title: 'test', detail: 't' * 1000 } }

      it 'if successfully responce return status 200 and error_code_0' do
        put "/todos/#{todo_id}", params: params
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'When the request is unusual' do
      it 'returns status 400 and error_code_2' do
        put "/todos/#{todo_id}", params: { title: '' }
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'When the request is unusual and title 101 or more charasters' do
      let(:params) { { title: 't' * 101 } }

      it 'returns status 400 and error_code_2' do
        put "/todos/#{todo_id}", params: params
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'When the request is unusual and title 1001 or more charasters' do
      let(:params) { { title: 'test', detail: 't' * 1001 } }

      it 'returns status 400 and error_code_2' do
        put "/todos/#{todo_id}", params: params
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'If the update fails' do
      it 'return status 500 and error_code_5' do
        allow_any_instance_of(Todo).to receive(:update!).and_raise(ActiveRecord::ActiveRecordError)
        put "/todos/#{todo_id}", params: valid_attributes
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 5
        expect(json['error_message']).to eq '更新に失敗しました'
      end
    end

    context 'When an unknown error' do
      it 'return status 500 and error_code_1' do
        allow_any_instance_of(Todo).to receive(:update!).and_raise(Exception)
        put "/todos/#{todo_id}", params: valid_attributes
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end

  describe 'DELETE /todos/:id' do
    context 'When the request is normal' do
      it 'if a success response,returned status 200 and error_code_0' do
        delete "/todos/#{todo_id}"
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'When there is no target ID' do
      before { Todo.find(todo_id).destroy }

      it 'return status 400 and error_code_2' do
        delete "/todos/#{todo_id}"
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'When delete failed' do
      it 'return status 500 and error_code_6' do
        allow_any_instance_of(Todo).to receive(:destroy!).and_raise(ActiveRecord::ActiveRecordError)
        delete "/todos/#{todo_id}"
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 6
        expect(json['error_message']).to eq '削除に失敗しました'
      end
    end

    context 'When an unknown error' do
      it 'return status 500 and error_code_1' do
        allow_any_instance_of(Todo).to receive(:destroy!).and_raise(Exception)
        delete "/todos/#{todo_id}"
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end
end